## PROJET DESCRIPTION
Il s'agit du projet ELFI du cours Conception Sonore US332F, où il faut ajouter tous les sons à un projet de jeu déjà existant.

## Que contiennent ces dossier ?

# tp elfi
Le dossier tpelfi contient le build du projet.

# Elfi
Le dossier elfi contient le projet Unity entier.
